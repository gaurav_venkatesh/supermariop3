package com.example.gauravvenkatesh.supermario;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.Rect;

/**
 * Created by Gaurav Venkatesh on 5/9/2015.
 */
public class MarioChar {
    private final SuperMarioSuperView smsv;
    Bitmap Mario,Mariomove1R,Mariomove2R,Mariomove1L,Mariomove2L;
    // What we know about the Mario Object
    int stage;
    int life;
    int x;
    int y;

    public MarioChar(SuperMarioSuperView smsv)
    {
        BitmapFactory.Options options = new BitmapFactory.Options();
        Mario = BitmapFactory.decodeResource(smsv.getResources(), R.drawable.standingmario, options);
        Mariomove1R = BitmapFactory.decodeResource(smsv.getResources(), R.drawable.supermario_mve1, options);
        Mariomove2R = BitmapFactory.decodeResource(smsv.getResources(), R.drawable.supermario_mve2, options);
        Mariomove1L = BitmapFactory.decodeResource(smsv.getResources(), R.drawable.supermario_mve1L, options);
        Mariomove2L = BitmapFactory.decodeResource(smsv.getResources(), R.drawable.supermario_mve2L, options);
        this.smsv=smsv;
        this.stage=stage;
        this.life=life;
        this.x=x;
        this.y=y;
    }

    public void drawMario(Canvas c)
    {
        // Simply creating the char
        Paint paint = new Paint();
        int Height = smsv.getHeight();
        int Width = smsv.getWidth();
        Rect MarioRect = new Rect(0,0,50,50);
        c.drawBitmap(Mario,null,MarioRect,paint);
    }

    public void MarioMove()
    {
        // To make the Mariochar Move
    }
}
