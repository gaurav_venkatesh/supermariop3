package com.example.gauravvenkatesh.supermario;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.Rect;

/**
 * Created by Gaurav Venkatesh on 5/9/2015.
 */
public class Background {

    private final SuperMarioSuperView smsv;
    Bitmap back,Dpad;

    public Background(SuperMarioSuperView smsv)
    {
        this.smsv=smsv;
        BitmapFactory.Options options = new BitmapFactory.Options();
        back = BitmapFactory.decodeResource(smsv.getResources(), R.drawable.background, options);
        Dpad = BitmapFactory.decodeResource(smsv.getResources(), R.drawable.dpad, options);
    }

    public void Draw(Canvas c)
    {
        //Draw The Background
        Paint paint = new Paint();
        int Height = smsv.getHeight();
        int Width = smsv.getWidth();
        Rect Backgr = new Rect(0,0,Width,Height);
        c.drawBitmap(back,null,Backgr,paint);

        //Draw the Dpad, Changes to be made to this function
        Paint paint1 = new Paint();
        paint.setAlpha(100);
        int Dpad_H = 50;
        int Dpad_W = 50;
        Rect Dpader = new Rect(Dpad_W,Dpad_H,Dpad_W+50,Dpad_H+50);
        c.drawBitmap(Dpad,null,Dpader,paint1);
    }

    public void MoveBack()
    {

    }
}
